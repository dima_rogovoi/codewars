package main

import (
	"fmt"
	"strings"
)

func LongestConsec(strarr []string, k int) string {
	if k <= 0 {
		return ""
	}
	var longestString string
	for i := 0; i < len(strarr)-k+1; i++ {
		s := strings.Join(strarr[i:i+k], "")
		if len(longestString) < len(s) {
			longestString = s
		}
	}
	return longestString
}

func main() {
	fmt.Println(LongestConsec([]string{}, 3))
}
