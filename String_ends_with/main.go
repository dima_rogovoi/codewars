package main

import "fmt"

func solution(str, ending string) bool {
	if str == "" && ending != "" {
		return false
	}
	j := 1
	for i := len(ending) - 1; i >= 0; i-- {
		if ending[i] != str[len(str)-j] {
			return false
		}
		j++
	}
	return true
}

func main() {
	fmt.Println(solution("", "t"))
}
