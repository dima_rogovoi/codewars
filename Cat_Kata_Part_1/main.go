package main

import "math"

type cat struct {
	x int
	y int
}

func distance(a cat, b cat) float64 {
	return math.Sqrt(float64((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y)))

}

func PeacefulYard(yard []string, minDistance int) bool {

	cats := make([]cat, 0)
	for i, s := range yard {
		for j, c := range s {
			if c != '-' {
				cats = append(cats, cat{x: i, y: j})
			}
		}
	}
	if len(cats) < 2 {
		return true
	}
	for i := 0; i < len(cats)-1; i++ {
		for j := i + 1; j < len(cats); j++ {
			if distance(cats[i], cats[j]) < float64(minDistance) {
				return false
			}
		}
	}
	return true
}

func main() {
	println(PeacefulYard([]string{
		"--------------------",
		"--------------------",
		"--------------------",
		"--------------------",
		"--------------------",
		"--------------------",
		"--------------------",
		"--------------------",
		"--R---L-------------",
		"--M-----------------"}, 2))
}
