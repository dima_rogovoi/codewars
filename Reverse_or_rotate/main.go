package main

import "strings"

func sumCube(s string) int {
	var total int32
	for _, symbol := range s {
		total += (symbol - '0') * (symbol - '0') * (symbol - '0')
	}
	return int(total)
}

func Rotate(s string) string {
	return s[1:] + s[:1]
}

func Reverse(s string) (result string) {
	for _, v := range s {
		result = string(v) + result
	}
	return result
}

func Revrot(s string, n int) string {
	if n == 0 {
		return ""
	}
	if len(s)%n != 0 {
		s = s[:len(s)-len(s)%n]
	}
	var result []string
	for i := 0; i < len(s); i += n {
		if sumCube(s[i:i+n])%2 == 0 {
			result = append(result, Reverse(s[i:i+n]))
		} else {
			result = append(result, Rotate(s[i:i+n]))
		}

	}
	return strings.Join(result, "")
}

func main() {
	println(Revrot("2225", 0))
}
