package main

import (
	"fmt"
	"strings"
)

func wave(words string) []string {
	result := make([]string, 0)
	word := strings.Split(words, "")
	for i := 0; i < len(word); i++ {
		if word[i] == " " {
			continue
		}
		word[i] = strings.ToUpper(word[i])
		result = append(result, strings.Join(word, ""))
		word[i] = strings.ToLower(word[i])
	}
	return result
}

func main() {
	fmt.Println(wave(" x yz"))
}
