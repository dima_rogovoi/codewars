package main

import "fmt"

func Solve(data [][]int) int {
	total := 1
	for _, s := range data {
		digits := make(map[int]struct{})
		for _, key := range s {
			digits[key] = struct{}{}
		}
		total *= len(digits)
	}
	return total
}

func main() {
	fmt.Println(Solve([][]int{{1, 2, 3}, {3, 4, 6, 6, 7}, {8, 9, 10, 12, 5, 6}}))
}
