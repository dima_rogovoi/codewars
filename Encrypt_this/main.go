package main

import (
	"fmt"
	"strconv"
	"strings"
)

func EncryptThis(text string) string {
	if text == "" {
		return ""
	}
	words := strings.Split(text, " ")
	for i, word := range words {
		b := []rune(word)
		w := strconv.Itoa(int(b[0]))
		if len(b) > 1 {
			b[1], b[len(b)-1] = b[len(b)-1], b[1]
			w += string(b[1:])
		}
		words[i] = w
	}
	return strings.Join(words, " ")
}

func main() {
	fmt.Println(EncryptThis(""))
}
