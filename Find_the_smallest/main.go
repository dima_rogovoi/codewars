package main

import (
	"fmt"
	"strconv"
)

func Smallest(n int64) []int64 {
	result := [3]int64{n, 0, 0}
	defer func() {
		fmt.Println(n, result)
	}()

	origin := strconv.Itoa(int(n))
	var from, to int64

	for i := 0; i < len(origin); i++ {
		for j := 0; j < len(origin); j++ {
			if i == j {
				continue
			}

			from = int64(i)
			to = int64(j)

			var part1, part2, part3, part4 string

			if to < from {
				part1 = origin[:to]
				part2 = string(origin[from])
				part3 = origin[to:from]
				part4 = origin[from+1:]
			} else {
				part1 = origin[:from]
				part2 = origin[from+1 : to+1]
				part3 = string(origin[from])
				part4 = origin[to+1:]
			}
			resStr := part1 + part2 + part3 + part4

			if v, _ := strconv.ParseInt(resStr, 10, 64); v < result[0] {
				result[0], result[1], result[2] = v, from, to
				if result[1]-result[2] == 1 {
					result[1], result[2] = result[2], result[1]
				}
			}
		}
	}
	return result[:]
}

func main() {
	fmt.Println(Smallest(30061204300103))
}
