package main

import (
	"fmt"
	"strings"
)

func DuplicateEncode(word string) string {
	var lowWord, result string
	lowWord = strings.ToLower(word)
	symbols := make(map[rune]int)
	for _, s := range lowWord {
		symbols[s] += 1
	}
	for _, j := range lowWord {
		if symbols[j] == 1 {
			result += "("
		} else {
			result += ")"
		}
	}
	return result
}

func main() {
	fmt.Println(DuplicateEncode("Succezz"))
}
