package main

import (
	"fmt"
	"strings"
)

func High(s string) string {
	strWord := strings.Split(s, " ")
	var maxTotal int
	var word string
	for i := 0; i < len(strWord); i++ {
		var total int
		for j := 0; j < len(strWord[i]); j++ {
			total += int(strWord[i][j]-'a'+1)
		}
		if total > maxTotal {
			maxTotal = total
			word = strWord[i]
		}
	}
	return word
}

func main() {
	fmt.Println(High("b aa"))
}
