package main

import "fmt"

func WhichNote(keyPressCount int) string {
	pianoKey1 := [12]string{
		"A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#",
	}

	lastKey := (keyPressCount - 1) % 88
	lastKey = lastKey % 12
	return pianoKey1[lastKey]

}

func main() {
	fmt.Println(WhichNote(89))
}
