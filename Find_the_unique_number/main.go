package main

import "fmt"

func FindUniq(arr []float32) float32 {
	for i := 0; i < len(arr); i++ {
		switch {
		case arr[i] != arr[i+1] && arr[i+1] == arr[i+2]:
			return arr[i]
		case arr[i] != arr[i+1] && arr[i+1] != arr[i+2]:
			return arr[i+1]
		case arr[i] == arr[i+1] && arr[i+1] != arr[i+2]:
			return arr[i+2]
		}
	}
	return arr[0]
}

func main() {
	fmt.Println(FindUniq([]float32{1.0, 1.0, 1.0, 0.55, 1.0, 1.0}))
}
