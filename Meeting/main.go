package main

import (
	"fmt"
	"sort"
	"strings"
)

func Meeting(s string) string {
	names := strings.Split(s, ";")
	resNames := make([]string, 0)
	for i := 0; i < len(names); i++ {
		s := strings.Split(names[i], ":")
		s[0], s[1] = strings.ToUpper(s[1]), strings.ToUpper(s[0])
		resNames = append(resNames, "("+s[0]+", "+s[1]+")")
	}
	sort.Strings(resNames)
	return strings.Join(resNames, "")
}

func main() {
	fmt.Println(Meeting("Alexis:Wahl;John:Bell;Victoria:Schwarz;Abba:Dorny;Grace:Meta;Ann:Arno;Madison:STAN;Alex:Cornwell;Lewis:Kern;Megan:Stan;Alex:Korn"))
}
