package main

import (
	"fmt"
	"strings"
)

var Alphabet = map[rune]string{
	'a': "alpha",
	'b': "bravo",
	'c': "charlie",
	'd': "delta",
	'e': "echo",
	'f': "foxtrot",
	'g': "golf",
	'h': "hotel",
	'i': "india",
	'j': "juliett",
	'k': "kilo",
	'l': "lima",
	'm': "mike",
	'n': "november",
	'o': "oscar",
	'p': "papa",
	'q': "quebec",
	'r': "romeo",
	's': "sierra",
	't': "tango",
	'u': "uniform",
	'v': "victor",
	'w': "whiskey",
	'x': "xray",
	'y': "yankee",
	'z': "zulu",
}

func ToNato(words string) string {
	var result []string
	singleWords := strings.Split(strings.ToLower(words), " ")
	for i := 0; i < len(singleWords); i++ {
		for _, j := range singleWords[i] {
			if v, ok := Alphabet[j]; ok {
				result = append(result, strings.Title(v))
				continue
			}
			result = append(result, string(j))
		}

	}
	return strings.Join(result, " ")
}

func main() {
	fmt.Println(ToNato("Mike Juliet"))
}
