package main

import (
	"fmt"
	"strconv"
	"strings"
)

func decipherThis(str string) string {
	words := strings.Split(str, " ")
	for i, word := range words {
		b := []rune(word)
		var index int
		for index < len(b) && b[index] >= '0' && b[index] <= '9' {
			index++
		}
		code, _ := strconv.Atoi(string(b[:index]))
		w := string(code)
		if index < len(b) {
			b[index], b[len(b)-1] = b[len(b)-1], b[index]
			w += string(b[index:])
		}
		words[i] = w
	}
	return strings.Join(words, " ")
}

func main() {
	fmt.Println(decipherThis("65 119esi 111dl 111lw 108dvei 105n 97n 111ka"))
}
