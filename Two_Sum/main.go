package main

import "fmt"

func TwoSum(numbers []int, target int) [2]int {
	result := [2]int{}
	for i := 0; i < len(numbers); i++ {
		for j := 1; j < len(numbers); j++ {
			if numbers[i]+numbers[j] == target {
				result[0] = i
				result[1] = j
				return result
			}
		}

	}
	return result
}

func main() {
	fmt.Println(TwoSum([]int{1234, 5678, 9012}, 14690))
}
