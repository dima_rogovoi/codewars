package main

import "fmt"

func Multiple3And5(number int) int {
	var total int
	if number <= 0 {
		total = 0
	}
	for i := number - 1; i > 0; i-- {
		if i%3 == 0 || i%5 == 0 {
			total += i
		}
	}
	return total
}
func main() {
	fmt.Println(Multiple3And5(10))
}
